
// dropdown

require([
'jquery'
], function ($, window, document) {

/*faq list show*/
    $(".faq_list_div ul li").click(function(){
    $(".faq_common_class").hide();
    $(".faq_list_div ul li").removeClass("active");
    $(this).addClass("active");
    var term = $(this).attr('data-id');
    var finalID = '#' + term;
    $(finalID).show();
});

/*faq-question answer toggle*/
$(".question_section_inner p.faq_ques").click(function(){
    var faqQ = $(this).attr('data-id');
    var faqA = '#' + faqQ;
    $(faqA).slideToggle();
    $(this).find('span').toggleClass('hide_show_icon');
});


$(".dropdown-menu li a").click(function(){
  var selText = $(this).text();
  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
});

$(document).ready(function(){

$('.otherSales .container .space').next('.space').hide();

}); 

$(document).ready(function(){
	$('.up-arrow').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
}); 


$(document).ready(function(){
$(".become_member").click(function(){
  $(this).parents('.access_link_box').hide();  
  $(".member-regstr").show();
  $(".member_login").show();
	});
 });  

$(document).ready(function(){
$(".logn").click(function(){
  $(this).parents('.member_login').hide();  
  $(".member-login").show();
  $(".member_regstr").show();
  $(".member-regstr").hide();
  $('.access_link_box').hide(); 
	});
	}); 

$(document).ready(function(){
$(".rgstr").click(function(){
  $(this).parents('.member_regstr').hide();  
  $(".member-regstr").show();
  $(".member_login").show();
  $(".member-login").hide();
	});
	}); 


 	$(document).ready(function(){
            $("#other-sell").owlCarousel({
                loop:true,
                autoPlay: 3000, //Set AutoPlay to 3 seconds
                animateIn: 'fadeIn',
                animateOut: 'fadeOut',
                items : 4,
               // itemsDesktop : [1199,2],
               // itemsDesktopSmall : [979,1]
              navigation : true,

    dots:false,
    responsive:{
      0:{
        items:1
      },
        479:{
            items:2
        },
        767:{
            items:3
        },
        1000:{
            items:4
        }
    }
            });
        
    });  


  $(document).ready(function(){
            $("#related").owlCarousel({
                loop:true,
               // autoPlay: 3000, //Set AutoPlay to 3 seconds
                animateIn: 'fadeIn',
                animateOut: 'fadeOut',
                items : 5,
               // itemsDesktop : [1199,2],
               // itemsDesktopSmall : [979,1]
              navigation : true,

    dots:false,
    responsive:{
      0:{
        items:1
      },
        479:{
            items:2
        },
        767:{
            items:3
        },
        1000:{
            items:5
        }
    }
            });
        
    });  



/* upender js */

$(document).ready(function(){
	$('.popupvochure').hide();
	$('.infoeartdiv').hide();
	$('#cartpopupvo').click(function(){
		
		$('.popupvochure').fadeIn();
	});
	$('.popupvochure .fa-times').click(function(){
		$('.popupvochure').fadeOut();
	});
	$('#cartinfo').click(function(){
		$('.infoeartdiv').fadeToggle();
	});




	 // tabbed content
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");
	


	$('.menu-left').click(function(){

		$(".menu-two").slideToggle();
		$(".to_loggedin_menu").slideToggle();

	});
	
});



$(document).ready(function(){

 /* var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	acc[i].addEventListener("click", function() {
		this.classList.toggle("active");
		var panel = this.nextElementSibling;
		if (panel.style.maxHeight){
		panel.style.maxHeight = null;
		} else {
		panel.style.maxHeight = panel.scrollHeight + "px";
		} 
	});
	}  */

	$(".toggle").click(function() {
      $("#menu").toggle();
    });

	$(".menu-left").click(function() {
     $(".menu").toggle();
   });

	$(".foot-mobile .accordion").each(function(){
		 $(this).click(function(){

         $(this).next(".panel").slideToggle("slow");
         $(this).toggleClass("active");
		});

	});

	});




});

